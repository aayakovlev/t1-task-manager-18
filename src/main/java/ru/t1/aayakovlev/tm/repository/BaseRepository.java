package ru.t1.aayakovlev.tm.repository;

import ru.t1.aayakovlev.tm.exception.entity.AbstractEntityException;
import ru.t1.aayakovlev.tm.exception.field.IdEmptyException;

import java.util.List;

public interface BaseRepository<T> {

    List<T> findAll();

    T findById(final String id) throws IdEmptyException;

    T remove(final T e) throws AbstractEntityException;

    T save(final T e);

}
