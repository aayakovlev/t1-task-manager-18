package ru.t1.aayakovlev.tm.repository;

import java.util.Comparator;
import java.util.List;

public interface ExtendedRepository<T> extends BaseRepository<T> {

    int count();

    void deleteAll();

    boolean existsById(final String id);

    List<T> findAll(final Comparator<T> comparator);

    T findByIndex(final Integer index);

    T removeById(final String id);

    T removeByIndex(final Integer index);

}
