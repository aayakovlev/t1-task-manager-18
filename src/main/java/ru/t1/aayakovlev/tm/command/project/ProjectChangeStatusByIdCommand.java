package ru.t1.aayakovlev.tm.command.project;

import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import java.util.Arrays;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    public static final String DESCRIPTION = "Change project status by id.";

    public static final String NAME = "project-change-status-by-id";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        System.out.println("Statuses to enter:");
        System.out.println(Arrays.toString(Status.values()));
        System.out.print("Enter new status: ");
        final String statusValue = nextLine();
        final Status status = Status.toStatus(statusValue);
        getProjectService().changeStatusById(id, status);
    }

}
