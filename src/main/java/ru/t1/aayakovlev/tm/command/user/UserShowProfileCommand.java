package ru.t1.aayakovlev.tm.command.user;

import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.model.User;

public final class UserShowProfileCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "Show user profile.";

    public static final String NAME = "user-show-profile";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW USER PROFILE]");
        final User user = serviceLocator.getAuthService().getUser();
        showUser(user);
        System.out.println("First name: " + user.getFirstName());
        System.out.println("Middle name: " + user.getMiddleName());
        System.out.println("Last name: " + user.getLastName());
        System.out.println("Role: " + user.getRole().getDisplayName());
    }

}
