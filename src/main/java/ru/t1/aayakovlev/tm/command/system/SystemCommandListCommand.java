package ru.t1.aayakovlev.tm.command.system;

import ru.t1.aayakovlev.tm.command.AbstractCommand;

import java.util.Collection;

public final class SystemCommandListCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-cmd";

    public static final String DESCRIPTION = "Show commands description.";

    public static final String NAME = "command";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMMAND LIST]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) {
            final String name = command.getName();
            final String description = command.getDescription();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name + ": " + description);
        }
    }

}
