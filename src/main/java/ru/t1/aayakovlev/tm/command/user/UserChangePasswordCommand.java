package ru.t1.aayakovlev.tm.command.user;

import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "Change user's password.";

    public static final String NAME = "user-change-password";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractFieldException {
        System.out.println("[CHANGE USER PASSWORD]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        System.out.print("Enter new password: ");
        final String password = nextLine();
        getUserService().setPassword(id, password);
    }

}
