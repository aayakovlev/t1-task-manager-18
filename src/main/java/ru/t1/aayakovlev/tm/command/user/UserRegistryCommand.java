package ru.t1.aayakovlev.tm.command.user;

import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.service.AuthService;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class UserRegistryCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "Register new user.";

    public static final String NAME = "user-registry";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER REGISTRY]");
        System.out.print("Enter login: ");
        final String login = nextLine();
        System.out.print("Enter password: ");
        final String password = nextLine();
        System.out.print("Enter email: ");
        final String email = nextLine();
        final AuthService authService = serviceLocator.getAuthService();
        final User user = authService.registry(login, password, email);
        showUser(user);
    }

}
