package ru.t1.aayakovlev.tm.command.system;

import ru.t1.aayakovlev.tm.command.AbstractCommand;

import java.util.Collection;

public final class SystemArgumentListCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-arg";

    public static final String DESCRIPTION = "Show argument description.";

    public static final String NAME = "argument";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENT LIST]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) {
            final String argument = command.getArgument();
            final String description = command.getDescription();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument + ": " + description);
        }
    }

}
