package ru.t1.aayakovlev.tm.service;

import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.model.Task;

import java.util.List;

public interface TaskService extends ExtendedService<Task> {

    Task create(final String name) throws AbstractFieldException;

    Task create(final String name, final String description) throws AbstractFieldException;

    List<Task> findByProjectId(final String projectId) throws AbstractFieldException;

    Task updateById(final String id, final String name, final String description) throws AbstractException;

    Task updateByIndex(final Integer index, final String name, final String description) throws AbstractException;

}
