package ru.t1.aayakovlev.tm.service;

import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;

import java.util.List;

public interface BaseService<T> {

    List<T> findAll();

    T findById(final String id) throws AbstractFieldException;

    T removeById(final String id) throws AbstractException;

    T save(final T e);

}
