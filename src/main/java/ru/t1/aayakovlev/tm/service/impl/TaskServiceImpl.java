package ru.t1.aayakovlev.tm.service.impl;

import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.TaskNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.*;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.repository.TaskRepository;
import ru.t1.aayakovlev.tm.service.TaskService;

import java.util.Comparator;
import java.util.List;

import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.FIRST_ARRAY_ELEMENT_INDEX;

public final class TaskServiceImpl implements TaskService {

    private final TaskRepository repository;

    public TaskServiceImpl(final TaskRepository repository) {
        this.repository = repository;
    }

    @Override
    public Task changeStatusById(final String id, final Status status) throws AbstractException {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (status == null) throw new StatusEmptyException();
        final Task task = findById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(final Integer index, final Status status) throws AbstractException {
        final int recordCount = repository.count();
        if (index == null || index < FIRST_ARRAY_ELEMENT_INDEX || index >= recordCount)
            throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();
        final Task task = findByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task create(final String name) throws AbstractFieldException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(name);
    }

    @Override
    public Task create(final String name, final String description) throws AbstractFieldException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(name, description);
    }

    @Override
    public void deleteAll() {
        repository.deleteAll();
    }

    @Override
    public List<Task> findAll() {
        return repository.findAll();
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public List<Task> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @Override
    public Task findById(final String id) throws AbstractFieldException {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        return repository.findById(id);
    }

    @Override
    public Task findByIndex(final Integer index) throws AbstractException {
        final int recordsCount = repository.count();
        if (index == null || index < FIRST_ARRAY_ELEMENT_INDEX || index >= recordsCount)
            throw new IndexIncorrectException();
        return repository.findByIndex(index);
    }

    @Override
    public List<Task> findByProjectId(final String projectId) throws AbstractFieldException {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        return repository.findAllByProjectId(projectId);
    }

    @Override
    public Task removeById(final String id) throws AbstractFieldException {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        return repository.removeById(id);
    }

    @Override
    public Task removeByIndex(final Integer index) throws AbstractFieldException {
        final int recordsCount = repository.count();
        if (index == null || index < FIRST_ARRAY_ELEMENT_INDEX || index >= recordsCount)
            throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

    @Override
    public Task save(final Task task) {
        if (task == null) return null;
        return repository.save(task);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) throws AbstractException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        final Task task = findById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) throws AbstractException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        final Task task = findByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

}
