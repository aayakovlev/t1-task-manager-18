package ru.t1.aayakovlev.tm.service;

import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.AbstractEntityException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.exception.field.EmailEmptyException;
import ru.t1.aayakovlev.tm.exception.field.LoginEmptyException;
import ru.t1.aayakovlev.tm.model.User;

public interface UserService extends BaseService<User> {

    User create(final String login, final String password) throws AbstractException;

    User create(final String login, final String password, final String email) throws AbstractException;

    User create(final String login, final String password, final Role role) throws AbstractException;

    User findByLogin(final String login) throws LoginEmptyException;

    User findByEmail(final String email) throws EmailEmptyException;

    User remove(final User user) throws AbstractEntityException;

    User removeByLogin(final String login) throws AbstractException;

    User removeByEmail(final String email) throws AbstractException;

    User setPassword(final String id, final String password) throws AbstractFieldException;

    User updateUser(final String id, final String firstname, final String lastName, final String middleName) throws AbstractException;

    boolean isLoginExists(final String login) throws LoginEmptyException;

    boolean isEmailExists(final String email) throws EmailEmptyException;

}
