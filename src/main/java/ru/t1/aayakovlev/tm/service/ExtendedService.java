package ru.t1.aayakovlev.tm.service;

import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;

import java.util.Comparator;
import java.util.List;

public interface ExtendedService<T> extends BaseService<T> {

    T changeStatusById(final String id, final Status status) throws AbstractException;

    T changeStatusByIndex(final Integer index, final Status status) throws AbstractException;

    void deleteAll() throws AbstractException;

    List<T> findAll(final Comparator<T> comparator);

    List<T> findAll(final Sort sort);

    T findByIndex(final Integer index) throws AbstractException;

    T removeByIndex(final Integer index) throws AbstractFieldException;

}
