package ru.t1.aayakovlev.tm.model;

import ru.t1.aayakovlev.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public final class Task implements WBS {

    private String id = UUID.randomUUID().toString();

    private Date created = new Date();

    private String description = "";

    private String name = "";

    private String projectId;

    private Status status = Status.NOT_STARTED;

    public Task() {
    }

    public Task(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

    public Task(final String name, final Status status) {
        this.name = name;
        this.status = status;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(final Date created) {
        this.created = created;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(final String projectId) {
        this.projectId = projectId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return name + " : " + description;
    }

}
